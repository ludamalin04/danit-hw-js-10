'use strict'
const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelector('.tabs-content');
const tabsLi = tabsContent.querySelectorAll('li');

tabsTitle.forEach(function (item) {
    item.addEventListener('click', function () {
        let activeTitle = item;
        let titleAttribute = activeTitle.getAttribute('data-tab');
        let activeLi = document.querySelector(titleAttribute);

        tabsTitle.forEach(function (item) {
            item.classList.remove('active');
        })
        tabsLi.forEach(function (item) {
            item.classList.remove('active-content');
        })
        activeTitle.classList.add('active');
        activeLi.classList.add('active-content');
    })
})
